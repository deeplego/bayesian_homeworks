import pandas as pd
import numpy as np
from pathlib import Path
import inspect
from scipy import stats
import matplotlib.pyplot as plt
import seaborn as sns
from tqdm import tqdm

RANDOM_STATE = 42
np.random.seed(RANDOM_STATE)
CURRENT_DIR = Path(inspect.getsourcefile(lambda: 0)).resolve().parent
DATA_DIR = CURRENT_DIR / 'data'
data = pd.read_table(DATA_DIR / 'msparrownest.dat',
        header=None, sep=' ', names=['label', 'wingspan'])
n_obs = data.shape[0]
data.insert(1, 'bias', np.ones(n_obs))
X = data.drop('label', axis=1).values
y = data['label']

def logistic(x):
    return np.exp(x - np.log(1 + np.exp(x)))

def sampling_model(beta):
    tmp = 0
    for i in range(n_obs):
        eta = np.matmul(X[i, :], beta)
        if y[i] == 1:
            tmp += eta - np.log(1 + np.exp(eta))
        else:
            tmp += np.log(1 + logistic(eta))
    return np.exp(tmp)

all_betas = []
delta = 0.001
prior_var_cov = [[delta, 0], [0, delta]]
beta = np.random.multivariate_normal([0, 0], prior_var_cov)
all_betas.append(beta)
n_iterations = 1000
eps = 1e-7
counter_acceptance = 0

for i in tqdm(range(n_iterations)):
    proposed = np.random.multivariate_normal(beta, prior_var_cov)
    num = sampling_model(proposed) * stats.multivariate_normal(
            beta, prior_var_cov).pdf(proposed)
    denom = sampling_model(beta) * stats.multivariate_normal(
            proposed, prior_var_cov).pdf(beta)
    r = np.exp(np.log(num + eps) - np.log(denom + eps))
    if (r > np.random.uniform()):
        beta = proposed
        counter_acceptance += 1
    all_betas.append(beta)
print('acceptance_ratio: {0:3f}'.format(counter_acceptance / n_iterations))

fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(19.2/2, 10.8/2))
ax1.plot(np.arange(n_iterations + 1), [x[0] for x in all_betas])
ax1.set_ylabel(r'$\alpha$    ', fontsize=14, rotation=0)
ax2.plot(np.arange(n_iterations + 1), [x[1] for x in all_betas])
ax2.set_ylabel(r'$\beta$    ', fontsize=14, rotation=0)
#plt.savefig(
#        '/home/lore/Computer_Science/Bayesian_Statistical_Inference/'
#        'homeworks/figures/10-2_trace_plots.png', format='png'
#)
plt.show()
fig, ax = plt.subplots(1, 1, figsize=(19.2/2, 10.8/2))

def logistic_plot(ax, beta):
    ax.plot(np.sort(X[:, 1]), logistic(np.matmul(np.sort(X, axis=0), beta)))
for beta in all_betas[1:]:
    logistic_plot(ax, beta)
ax.set_ylabel(r'$\hat{y}$    ', rotation=0)
ax.set_xlabel('x', rotation=0)
#plt.savefig(
#        '/home/lore/Computer_Science/Bayesian_Statistical_Inference/'
#        'homeworks/figures/10-2_bands.png', format='png'
#)
plt.show()

