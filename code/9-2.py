#%%
import copy
import inspect
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from path import Path
from pprint import pprint
from string import punctuation, whitespace

plt.rcParams["figure.figsize"] = (16, 8)

CURRENT_DIR = Path(inspect.getsourcefile(lambda: 0)).abspath().parent
DATA_DIR = CURRENT_DIR / "data"
file_path = DATA_DIR / "azdiabetes.csv"

#%% read original .dat file and convert it to .csv or read .csv if done
if not file_path.exists():
    file_path = DATA_DIR / "azdiabetes.dat"
    with open(file_path, "r") as f:
        columns = [x.strip() for x in f.readline().split(" ")]
        txt = f.read()

    txt_splitted = [
        x.strip() for x in txt.split(" ") if x not in punctuation + whitespace
    ]
    data_values = []
    n_cols = 8
    n_rows = 532  # taken with cmd wc -l azdiabetes.dat
    for i in range(n_rows):
        row = []
        for j in range(n_cols):
            row.append(txt_splitted[i * n_cols + j])
        data_values.append(row)

    data = pd.DataFrame(data_values, columns=columns)
    data.to_csv(DATA_DIR / "azdiabetes.csv", index=False)
else:
    data = pd.read_csv(file_path).drop("diabetes", axis=1)

# %%
data.insert(0, "bias", 1)
X = data.drop("glu", axis=1).values
y = data["glu"].values
n, p = X.shape
g = n
nu0 = 2
s20 = 1

beta = np.repeat(0, p)
sigma = s20
beta_buffer = [copy.deepcopy(beta)]
sigma_buffer = [copy.deepcopy(sigma)]

S = 1_000
for s in range(S):
    beta_mu = (
        g
        / (g + 1)
        * np.matmul(
            np.matmul(np.linalg.inv(np.matmul(np.transpose(X), X)), np.transpose(X)), y
        )
    )
    beta_var = g / (g + 1) * sigma * np.linalg.inv(np.matmul(np.transpose(X), X))
    beta = np.random.multivariate_normal(beta_mu, beta_var)

    ssr = np.matmul(np.transpose(y), y) - np.matmul(
        np.matmul(np.transpose(beta_mu), np.linalg.inv(beta_var)), beta_mu
    )
    sigma = 1 / np.random.gamma(nu0 + n, 1 / ((nu0 * s20 + ssr) / 2))
    beta_buffer.append(copy.deepcopy(beta))
    sigma_buffer.append(copy.deepcopy(sigma))

for idx in range(p):
    # plt.boxplot([beta[idx] for beta in beta_buffer], positions=[idx])
    plt.plot([beta[idx] for beta in beta_buffer])
    plt.hlines(0, 0, 10)
plt.legend(np.arange(p))

for idx in range(p):
    x = np.sort([beta[idx] for beta in beta_buffer])
    print(f"CI beta_{idx}: ({x[round(S * 0.025)]}, {x[round(S * 0.975)]})")
plt.show()
#%%
odds = np.repeat(1, p)
zeta = np.repeat(1, p)
sigma = s20
beta = np.repeat(1, p)
sigma_buffer = []
beta_buffer = []
zeta_buffer = []


def custom_ssr(crit, Xz):
    result = np.matmul(
        np.matmul(
            np.transpose(y),
            np.subtract(
                np.identity(np.sum(n)),
                n
                / (n + 1)
                * np.matmul(
                    np.matmul(Xz, np.linalg.inv(np.matmul(np.transpose(Xz), Xz))),
                    np.transpose(Xz),
                ),
            ),
        ),
        y,
    )
    return result


Xz = X[:, list(map(bool, zeta))]

for s in range(50):
    for j in range(p):
        z = [x for idx, x in enumerate(zeta) if idx != j]
        za = z
        zb = z
        za.insert(j, 1)
        zb.insert(j, 0)
        first = (1 + n) ** ((np.sum(zb) - np.sum(zb)) / 2)
        second = (np.var(za) / np.var(zb)) ** (1 / 2)
        third = (
            (np.var(zb) + custom_ssr(zb, Xz)) / (np.var(za) + custom_ssr(za, Xz))
        ) ** ((n + 1) / 2)
        odds[j] = first * second * third
    zeta = [1] + [np.random.binomial(1, odds[j] / (1 + odds[j])) for j in range(1, p)]

    Xz = X[:, list(map(bool, zeta))]
    #    Xz = []
    #    for idx in range(n):
    #        row = []
    #        for ii in range(len(zeta)):
    #            if zeta[ii] == 1:
    #                row.append(X[idx, ii])
    #        Xz.append(row)
    beta_mu = (
        g
        / (g + 1)
        * np.matmul(
            np.matmul(np.linalg.inv(np.matmul(np.transpose(Xz), Xz)), np.transpose(Xz)),
            y,
        )
    )
    beta_var = g / (g + 1) * sigma * np.linalg.inv(np.matmul(np.transpose(Xz), Xz))
    beta = np.random.multivariate_normal(beta_mu, beta_var)

    ssr = np.matmul(np.transpose(y), y) - np.matmul(
        np.matmul(np.transpose(beta_mu), np.linalg.inv(beta_var)), beta_mu
    )
    sigma = 1 / np.random.gamma(nu0 + n, 1 / ((nu0 * s20 + ssr) / 2))

    beta = beta.tolist()
    _ = [beta.insert(idx, 0) for idx, z in enumerate(zeta) if z == 0]

    zeta_buffer.append(copy.deepcopy(zeta_buffer))
    beta_buffer.append(copy.deepcopy(beta))
    sigma_buffer.append(copy.deepcopy(sigma))


from pprint import pprint

print(beta_buffer)
for idx in range(p):
    plt.plot([beta[idx] for beta in beta_buffer])
plt.legend(np.arange(p))
plt.show()
